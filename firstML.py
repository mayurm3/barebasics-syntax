#####
#
# First ML program reference from machinelearningmastery.com Jason Brownlee
#
#####

# Print Date Time
from customUtils import loggingUtils
#loggingUtils.printDate()
loggingUtils.printDateTime()


# Load libraries
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

# Load dataset
url = "datasets/iris.csv"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pandas.read_csv(url, names=names)

# shape
print ("\nDataset Shape :\n")
print(dataset.shape)

# head
print ("\nDataset Head 10 rows :\n")
print(dataset.head(10))

# descriptions
print ("\nDataset Descriptions (Describe) :\n")
print(dataset.describe())

# class distribution
print ("\nDataset size of each groupping when Groupedby Class :\n")
print(dataset.groupby('class').size())

def plotBoxWisker():
	# box and whisker plots
	print ("\nBox and Wisker plot of the dataset :\n")
	dataset.plot(kind='box', subplots=True, layout=(2,2), sharex=False, sharey=False)
	plt.show()

def plotHistogram():
	# histograms
	print ("\nHistogram of the dataset :\n")
	dataset.hist()
	plt.show()

def plotScatterPlot():
	# scatter plot matrix
	print ("\nScatter Plot Matrix of the dataset :\n")
	scatter_matrix(dataset)
	plt.show()

#plotBoxWisker()
#plotHistogram()
#plotScatterPlot()

# Split-out validation dataset
array = dataset.values
X = array[:,0:4]
Y = array[:,4]
validation_size = 0.20
seed = 7
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)

# Test options and evaluation metric
seed = 7
scoring = 'accuracy'

# Spot Check Algorithms
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))

# Evaluate each model in turn
results = []
names = []
for name, model in models:
	kfold = model_selection.KFold(n_splits=10, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	print(msg)

def plotComparision():
	# Compare Algorithms Graphically
	fig = plt.figure()
	fig.suptitle('Algorithm Comparison')
	ax = fig.add_subplot(111)
	plt.boxplot(results)
	ax.set_xticklabels(names)
	plt.show()

#plotComparision()

# Make predictions on validation dataset
knn = KNeighborsClassifier()
knn.fit(X_train, Y_train)
predictions = knn.predict(X_validation)
print("\n Accuracy Score :",accuracy_score(Y_validation, predictions))
print("\n Confusion Matrix :\n",confusion_matrix(Y_validation, predictions))
print("\n Classification Report :\n",classification_report(Y_validation, predictions))
